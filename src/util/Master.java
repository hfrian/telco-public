package util;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

public class Master {
	public static void main(String args[]) {
		new Master().testPrintText();
		
		for(int i=0; i<args.length; i++) {
			System.out.println("args[" +i +"] : " +args[i]);	
		}
		
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testNG = new TestNG();
		testNG.setTestClasses(new Class [] {TestGoogle.class});
		testNG.addListener(tla);
		testNG.run();
	}
	
	public void testPrintText() {
		System.out.println("telco public");
	}
}
